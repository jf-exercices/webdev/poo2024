<?php

// chargement auto des classes
spl_autoload_register();

use app\controllers\UserController;

// Appel de la méthode STATIQUE getById.
// Pas besoin d'intancier un objet dans ce cas.
$user1 = UserController::getById(1);
// $user1 est bien une instance de UserController,
// car getByID retourne un objet de type UserController
echo '<br>Email user id 1 : ' . $user1->getEmail();
$user2 = UserController::getById(2);
echo '<br>Email user id 2 : ' . $user2->getEmail();
var_dump($user1);
// création d'utilisateur invalide car firstname et lastname de -3 char
$user = new UserController(99,
    't',
    't',
    'test@toto.com',
    'toto',
    '127.0.0.1');
$user->create();


// Utiliser les Getters et les Setters

// Créer une nouvelle méthode permettant de valider les données utilisateur
// Cette méthode doit être appelée lors de chaque instanciation

// Créer une classe User.php dans models permettant de se connecter à la table user de notre DB

// Créer une méthode permettant d'insérer un utilisateur en DB
// Créer une méthode permettant de récupérer l'ensemble des champs DB et d'en affecter les valeurs aux attributs de la classe
// Créer une méthode permettant de récupérer un utilisateur en DB sur base de son id
// Créer une méthode permettant de récupérer plusieurs utilisateurs en DB sur base de la valeur d'un champ
// Créer une méthode permettant de mettre à jour un utilisateur en DB