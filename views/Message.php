<?php

namespace views;

class Message
{
    public static function message(string $message): void
    {
        echo '<p>' . $message . '</p>';
    }
}