<?php
namespace app\controllers;

use views\Message;
use app\models\User;

class UserController
{
    private int $id;
    private string $firstname;
    private string $lastname;
    private string $email;
    private string $password;
    private string $ip;
    private bool $status = false;
    private string $created = '0000-00-00 00:00:00';

    /**
     * Constructeur : INSTANCIE un OBJET sur base de la CLASSE
     * Cette méthode est appelée à chaque fois que
     * "new UserController()" est exécuté.
     *
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $password
     * @param string $ip
     */
    public function __construct(string $firstname, string $lastname, string $email, string $password, string $ip) {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->password = $password;
        $this->ip = $ip;
    }

    public static function getById(int $id): UserController|false
    {
        $user = User::getById($id);
        if (is_object($user)) {
            $userC = new UserController(
                $user->firstname,
                $user->lastname,
                $user->email,
                $user->password,
                $user->ip,
            );
            $userC->setId($user->id);
            return $userC;
        } else {
            return false;
        }
    }

    public function create(): void
    {
        // Appel de la méthode validateEmail

        // sur l'INSTANCE de l'OBJET UserController ($this)
        if ($this->validateEmail($this->email)
            && $this->validateName($this->firstname)
            && $this->validateName($this->lastname)) {
            // Appel de la méthode STATIQUE create de la CLASSE User
            User::create($this);
        } else {
            // Appel de la méthode STATIQUE message de la CLASSE message
            Message::message('Email is not valid');
        }

    }

    public function setEmail(string $email): void
    {
        // Validation de l'email avant la modification de l'attribut
        if ($this->validateEmail($email)) {
            $this->email = $email;
        }
    }

    /**
     * Validation d'un email
     *
     * @param string $email
     * @return bool
     */
    private function validateEmail(string $email): bool
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    private function validateName(string $name): bool
    {
        if (strlen($name) >= 3) {
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        if ($this->validateName($firstname)) {
            $this->firstname = $firstname;
        }
    }

    /**
     * @param string $name
     */
    public function setLastname(string $name): void
    {
        if ($this->validateName($name)) {
            $this->lastname = $name;
        }
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }


}