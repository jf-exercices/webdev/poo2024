<?php

namespace app\models;

use PDO;

require_once __DIR__ . '/../../config.php';

class Model
{
    public static function connect(): PDO
    {
        return new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST
            , DB_USER, DB_PASSWORD);
    }

    /**
     * Récupère la classe appelante
     * (classe enfant de Model, correspondant au nom de la table en DB)
     *
     * @return string
     */
    protected static function getClassName(): string
    {
        $class = get_called_class();
        $data = explode('\\', $class);
        return strtolower(end($data));
    }

    /**
     * Récupère la liste des champs de la table en DB
     * correspondant au nom de la classe appelante
     *
     * @return array
     */
    protected static function getColumns()
    {
        $columns = [];
        $cols = self::connect()->query("DESCRIBE " . self::getClassName(), PDO::FETCH_OBJ);
        foreach ($cols as $col) {
            $columns[] = $col->Field;
        }
        return $columns;
    }

}