<?php

namespace app\models;

use app\controllers\UserController;

class User extends Model
{

    /**
     * Récupère un user en DB sur base de son id
     *
     * @param int $id
     * @return object|\$0|false|\stdClass|null
     */
    public static function getById(int $id): object
    {
        $dbh = self::connect();
        $query = "SELECT * FROM user WHERE id = ?";
        $stmt = $dbh->prepare($query);
        $stmt->execute([$id]);
        return $stmt->fetchObject();
    }

    /**
     * Récupère un user en DB sur base d'un champ et de sa valeur
     *
     * @param string $field
     * @param string $value
     * @return object|false
     */
    public static function getByField(string $field, string $value): object|false
    {
        // Si le champ n'existe pas, FALSE est renvoyé
        if (!in_array($field, self::getColumns())) {
            return false;
        }
        $dbh = self::connect();
        $query = "SELECT * FROM user WHERE $field = ?";
        $result = $dbh->prepare($query);
        $result->execute([$value]);
        return $result->fetchObject();
    }

    /**
     * Création de l'utilisateur en DB
     *
     * @param UserController $user
     * @return bool
     */
    public static function create(UserController $user): bool
    {
        $dbh = self::connect();
        $query = "INSERT INTO user 
            (firstname, lastname, email, password, ip, status, created) 
            VALUES (?, ?, ?, ?, ?, 0, NOW())";
        $params = [
            $user->getFirstname(),
            $user->getLastname(),
            $user->getEmail(),
            password_hash($user->getPassword(), PASSWORD_DEFAULT), //cryptage
            $user->getIp(),
        ];
        $sql = $dbh->prepare($query);
        $sql->execute($params);
        $userid = $dbh->lastInsertId();
        if (!empty($userid)) {
            return true;
        } else {
            return false;
        }

    }
}